(function ( $ ) {
    "use strict";

    $(function () {
        var masterslider_d1da = new MasterSlider();

        // slider controls
        masterslider_d1da.control('arrows'     ,{ autohide:true, overVideo:true  });
        masterslider_d1da.control('bullets'    ,{ autohide:false, overVideo:true, dir:'h', align:'bottom', space:6 , margin:25  });
        // slider setup
        masterslider_d1da.setup("slider_1", {
            width           : 1140,
            height          : 800,
            minHeight       : 0,
            space           : 0,
            start           : 1,
            grabCursor      : false,
            swipe           : true,
            mouse           : true,
            keyboard        : true,
            layout          : "fullwidth",
            wheel           : false,
            autoplay        : false,
            instantStartLayers:false,
            mobileBGVideo:false,
            loop            : true,
            shuffle         : false,
            preload         : 0,
            heightLimit     : true,
            autoHeight      : false,
            smoothHeight    : true,
            endPause        : false,
            overPause       : true,
            fillMode        : "fill",
            centerControls  : true,
            startOnAppear   : false,
            layersMode      : "center",
            autofillTarget  : "",
            hideLayers      : false,
            fullscreenMargin: 0,
            speed           : 20,
            dir             : "h",
            parallaxMode    : 'swipe',
            view            : "basic"
        });



        $("head").append( "<link rel='stylesheet' id='ms-fonts'  href='http://fonts.googleapis.com/css?family=Montserrat:regular,700%7CCrimson+Text:regular' type='text/css' media='all' />" );

        window.masterslider_instances = window.masterslider_instances || {};
        window.masterslider_instances["5_d1da"] = masterslider_d1da;
     });

})(jQuery);

$('#contact-form').submit(function(e){
    e.preventDefault();

    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        beforeSend: function(data) {
            $('#gif-contato').css('display', 'block');
            $("#contact-form :input").prop("disabled", true);
        },
        success: function(data) {
            $('#gif-contato').css('display', 'none');
            $('#status-msg').text('Mensagem enviada com sucesso!')
        },
        error: function(data) {
            $('#gif-contato').css('display', 'none');
            $('#status-msg').text('Falha ao enviar mensagem. Tente novamente mais tarde.')
        }
    })
});

$('#newsletter-form').submit(function(e){
    e.preventDefault();

    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: $(this).serialize(),
        success: function(data) {
            $('#status-msg').text('Seu e-mail foi cadastrado com sucesso!')
            $("#status-msg").fadeIn("slow");
            $("#newsletter-form :input").prop("disabled", true);
        },
        error: function(data) {
            $('#status-msg').text('Falha ao cadastrar e-mail. Tente novamente mais tarde.')
            $("#status-msg").fadeIn("slow");
            $("#newsletter-form :input").prop("disabled", true);
        }
    })
});

$(document).ready(function(){
    $("#telefone").mask("(00) 0000-00009");
});

$(document).ready(function(){

    $('.your-class').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
    });

});


