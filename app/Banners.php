<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banners extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'image',
        'title',
        'path',
        'status',
        'order',
        'text_1',
        'font_color_1',
        'font_weight_1',
        'text_2',
        'font_color_2',
        'font_weight_2',
        'button_link',
        'button_text',
        'button_bg_color',
        'button_text_color',
        'text_align',
    ];

    public $rules = [
        'image' => 'required',
        'title' => 'required',
    ];
    public $rules_update = [
        'title' => 'required',
    ];
}
