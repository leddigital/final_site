<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Informations extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'address',
        'number',
        'complement',
        'district',
        'zipcode',
        'city',
        'state',
        'email',
        'phone1',
        'about_us',
        'video',
        'opening_hours',
    ];
}
