<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    public $timestamp = true;

    protected $fillable = [
        'title',
        'short_description',
        'content',
        'url',
        'image',
        'feat_image'
    ];

    public $rules = [
        'title' => 'required',
        'short_description' => 'required',
        'content' => 'required',
        'url' => 'required',
        'image' => 'required',
        'feat_image' => 'required'
    ];

}
