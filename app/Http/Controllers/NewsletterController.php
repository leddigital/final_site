<?php

namespace App\Http\Controllers;

use App\Newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{

    protected $model;

    public function __construct()
    {
        $this->model = new Newsletter();
    }

    public function cadastrar(Request $request) {
        $form = $request->all();

        if($this->model->create($form)) echo "0";
        else echo "1";

    }



}
