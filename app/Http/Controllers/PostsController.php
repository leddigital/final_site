<?php

namespace App\Http\Controllers;

use App\Posts;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Posts();
        setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    }

    public function index() {
        return view('admin.content.posts.index');
    }

    public function form(Request $request) {

        $id = $request->route('id');
        if(isset($id) && $id != "") {
            $entity = $this->model->where('id', '=', $id)->get()->first();
            return view('admin.content.posts.form', ['entity' => $entity]);
        } else
            return view('admin.content.posts.form');

    }

    public function readAll(Request $request) {

        $collection = $this->model->get()->all();
        $data['data'] = $collection;
        echo json_encode($data);

    }

    public function save (Request $request) {

        $postPath = "/img/bionews/";
        $form = $request->all();
        $id = $request->route('id');

        if(isset($id) && $id != "") {

            $entity = $this->model->find($id);

            if(isset($form['base64']) && $form['base64']!="")
                $form['image'] = $this->saveImg($form['base64'], 'post_', $postPath, $entity->image);

            if(isset($form['base64_min']) && $form['base64_min']!="")
                $form['feat_image'] = $this->saveImg($form['base64_min'], 'post_min_', $postPath, $entity->feat_image);

            if($entity->update($form)) {

                $res = [
                    'status' => 200,
                    'data' => $entity,
                ];
            } else {
                $res = [
                    'status' => 500,
                    'data' => $entity,
                ];
            }

        } else {

            $form['feat_image'] = $this->saveImg($form['base64_min'], 'post_min_', $postPath);
            $form['image'] = $this->saveImg($form['base64'], 'post_', $postPath);

            $form['url'] = $this->url_verify($form['title'], $this->model);

            if($entity = $this->model->create($form)){

                $res = [
                    'status' => 200,
                    'data' => $entity,
                ];

            } else {

                $res = [
                    'status' => 500,
                    'data' => $entity,
                ];

            }

        }
        return response()->json($res);
    }

    public function delete(Request $request) {

        $postPath = "/img/bionews/";
        $id = $request->route('id');
        $entity = $this->model->find($id);

        if($entity->delete()) {
            @unlink(public_path() . $postPath . $entity->feat_image);
            @unlink(public_path() . $postPath . $entity->image);
        }

    }

}
