<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    public $timestamp = true;

    protected $fillable = [
        'email'
    ];

    public $rules = [
        'email' => 'required'
    ];
}
