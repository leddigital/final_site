@extends('layouts.site')
@section('title', 'Sobre a Bionat')
@section('page-title', 'BIONAT')
@section('page-subtitle', 'Conheça um pouco da nossa história.')
@section('content')

<div class="body-wrapper float-menu">

    <!-- is search -->
    <div class="content-wrapper">
        <div class="greennature-content">

            <!-- Above Sidebar Section-->

            <!-- Sidebar With Content Section-->
            <div class="with-sidebar-wrapper">

                @include('blocks.mainbanner')

                <section id="content-section-2">
                    <div class="greennature-color-wrapper  gdlr-show-all no-skin greennature-half-bg-wrapper" style="background-color: #ffffff; ">
                        <div class="greennature-half-bg greennature-bg-solid custom-bg" style="background-image: url('{{ asset('img/bionat.jpg') }}');"></div>
                        <div class="container">
                            <div class="six columns"></div>
                            <div class="six columns">
                                <div class="greennature-item greennature-about-us-item greennature-normal">
                                    <div class="about-us-title-wrapper">
                                        <h3 class="about-us-title">GERAR SOLUÇÕES BIOLÓGICAS NATURAIS, SUSTENTÁVEIS, INTELIGENTES E EFICIENTES PARA O AGRO.</h3>
                                        {{-- <div class="about-us-caption greennature-title-font greennature-skin-info">Amet Dapibus Mollis</div> --}}
                                        <div class="about-us-title-divider"></div>
                                    </div>
                                    <div class="about-us-content-wrapper">
                                        <div class="about-us-content greennature-skin-content">
                                            <p> Nossa estrutura fabril usa tecnologia de ponta associada a processos produtivos rigorosos,
                                                rastreáveis, eficientes e específicos para a produção de agentes biológicos de alta qualidade,
                                                reconhecidos por vários cientistas da área.
                                            </p>
                                            <p> A Bionat trabalha em parceria com as maiores e mais conceituadas empresas de pesquisas microbiológicas do Brasil,
                                                para trazer produtos inéditos e inovadores ao mercado. Trabalhamos fortemente com a tecnologia de aplicação,
                                                que busca potencializar nossas soluções.
                                            </p>
                                            <p>Todos os processos são conduzidos por agrônomos e biólogos especializados no assunto.</p>
                                            <p>É alta tecnologia e qualificação para melhorar a produtividade no campo, sem agredir o meio ambiente</p>
                                        </div>
                                        {{-- <a class="about-us-read-more greennature-button" href="#">Read More</a> --}}
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>



                <section class="section-container container" style="max-width:1000px">


                    <div class="your-class">
                        <div><img src="{{ asset('img/galeria/B1.jpg') }}" alt=""></div>
                        <div><img src="{{ asset('img/galeria/B2.jpg') }}" alt=""></div>
                        <div><img src="{{ asset('img/galeria/B3.jpg') }}" alt=""></div>
                        <div><img src="{{ asset('img/galeria/B4.jpg') }}" alt=""></div>
                        <div><img src="{{ asset('img/galeria/B5.jpg') }}" alt=""></div>
                    </div>


                </section>

                {{-- @include('blocks.bioforcas') --}}

            </div>
            <!-- Below Sidebar Section-->

        </div>
        <!-- greennature-content -->
        <div class="clear"></div>
    </div>
    <!-- content wrapper -->

</div>

@endsection
