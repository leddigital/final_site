@extends('layouts.site')
@section('title', 'MIP - Bionat')
@section('page-title', 'MIP')
@section('page-subtitle', 'Manejo Integrado de Pragas')
@section('content')

<div class="body-wrapper float-menu">

    <!-- is search -->
    <div class="content-wrapper">
        <div class="greennature-content">

            <!-- Above Sidebar Section-->

            <!-- Sidebar With Content Section-->
            <div class="with-sidebar-wrapper">

                @include('blocks.mainbanner')

                <section id="content-section-2">
                    <div class="greennature-color-wrapper  gdlr-show-all no-skin greennature-half-bg-wrapper" style="background-color: #ffffff; ">
                        <div class="greennature-half-bg greennature-bg-solid custom-bg" style="background-image: url('{{ asset('img/mip.jpg') }}');"></div>
                        <div class="container">
                            <div class="six columns"></div>
                            <div class="six columns">
                                <div class="greennature-item greennature-about-us-item greennature-normal">
                                    <div class="about-us-title-wrapper">
                                        <h3 class="about-us-title">MIP é manejo integrado de pragas e doenças.</h3>
                                        {{-- <div class="about-us-caption greennature-title-font greennature-skin-info">Amet Dapibus Mollis</div> --}}
                                        <div class="about-us-title-divider"></div>
                                    </div>
                                    <div class="about-us-content-wrapper">
                                        <div class="about-us-content greennature-skin-content">
                                            <p>
                                                O manejo integrado de pragas e doenças é uma estratégia de controle múltiplo de infestações que se
                                                fundamenta no controle ecológico e nos fatores de mortalidade naturais procurando desenvolver táticas de
                                                controle que interfiram minimamente com esses fatores com o objetivo de diminuir as chances dos insetos
                                                ou doenças de se adaptarem a alguma prática defensiva em especial.
                                            </p>
                                            <p>
                                                Quando bem empregada, a técnica do Manejo Integrado de Pragas e Doenças (MIP) limita os efeitos potenciais
                                                prejudiciais dos pesticidas químicos à saúde e ao meio ambiente. 
                                            </p>
                                            <p>
                                                O objetivo dessa estratégia não é o de eliminar os agentes, mas reduzir sua população de modo a permitir que seus inimigos
                                                naturais permaneçam na plantação agindo sobre suas presas favorecendo a volta do equilíbrio natural desfeito pela plantação e pelo uso de defensivos agrícolas. Dessa forma, requer o entendimento do sistema da plantação como um todo e o conhecimento das interelações ecológicas entre os insetos agressores, seus inimigos naturais e o ambiente onde está a plantação está inserida.</p>
                                        </div>
                                        {{-- <a class="about-us-read-more greennature-button" href="#">Read More</a> --}}
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>

                {{-- @include('blocks.bioforcas') --}}

            </div>
            <!-- Below Sidebar Section-->

        </div>
        <!-- greennature-content -->
        <div class="clear"></div>
    </div>
    <!-- content wrapper -->

</div>

@endsection
