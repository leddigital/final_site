@extends('layouts.site')
@section('title', 'Bioforças - Bionat')
@section('content')

<div class="body-wrapper float-menu">

    <div class="content-wrapper">
        <div class="greennature-content">

            <div class="with-sidebar-wrapper">
                <div class="with-sidebar-container container greennature-class-no-sidebar">
                    <div class="with-sidebar-left twelve columns">
                        <div class="with-sidebar-content twelve columns">
                            <div class="greennature-item greennature-portfolio-style2 greennature-item-start-content">
                                <div id="portfolio-76" class="post-76 portfolio type-portfolio status-publish has-post-thumbnail hentry portfolio_category-environment portfolio_category-volunteer portfolio_tag-donation portfolio_tag-volunteer">
                                    <div class="greennature-portfolio-thumbnail thumb-personalizada">
                                        <div class="greennature-stack-image-wrapper">
                                            <div class="greennature-stack-image">
                                                <a href="{{ asset('img/bioforcas/'.$bioforca->image) }}" data-fancybox-group="greennature-gal-1" data-rel="fancybox"><img src="{{ asset('img/bioforcas/'.$bioforca->image) }}" alt="" width="1280" height="853" /></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="greennature-portfolio-content">
                                        <div class="greennature-portfolio-info">
                                            <h4 class="head">Informações complementares</h4>

                                            <div class="content">
                                                <div class="portfolio-info portfolio-clients">
                                                    <span class="info-head greennature-title">Nome </span>{{ $bioforca->title }}</div>
                                                <div class="portfolio-info portfolio-skills"><span class="info-head greennature-title">Embalagem </span>{{ $bioforca->embalagem }}</div>
                                                <div class="portfolio-info portfolio-website"><span class="info-head greennature-title">Categoria </span>{{ $bioforca->categoria }}</div>
                                                <div class="portfolio-info portfolio-website"><span class="info-head greennature-title">Concentração </span>{{ $bioforca->concentracao }}</div>
                                                <div class="portfolio-info portfolio-website"><span class="info-head greennature-title">Composição </span><i>{{ $bioforca->composicao }}</i></div>
                                                <div class="portfolio-info portfolio-website"><span class="info-head greennature-title">Recomendacão </span>{{ $bioforca->recomendacao }}</div>

                                                <div class="clear"></div>

                                            </div>
                                        </div>


                                    </div>

                                    <a class="action-ads-button large greennature-button" href="{{ asset('img/bioforcas/bulas/sample.pdf') }}"
                                    style="color: #6d5b1c;background-color: #fec428;" download>
                                    <i class="fa fa-download" aria-hidden="true"></i> Baixar Bula</a>

                                </div>

                                <div class="clear"></div>

                                <div class="six columns">
                                    <article class="bio-descricao">
                                        {!! $bioforca->content !!}
                                    </article>
                                </div>

                            </div>

                        </div>

                        <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>

        </div>
        <!-- greennature-content -->
        <div class="clear"></div>
    </div>
    <!-- content wrapper -->

@endsection
