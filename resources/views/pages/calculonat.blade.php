@extends('layouts.site')
@section('title', 'Contato - Bionat')
@section('content')

<div class="body-wrapper  float-menu" data-home="https://demo.goodlayers.com/greennature/">

    <!-- is search -->
    <div class="content-wrapper">
        <div class="greennature-content">

            <!-- Above Sidebar Section-->

            <!-- Sidebar With Content Section-->
            <div class="with-sidebar-wrapper">

                <section id="content-section-4">
                    <div class="greennature-color-wrapper  gdlr-show-all no-skin" style="background-color: #ffffff; padding-top: 70px; padding-bottom: 35px; ">
                        <div class="container">
                            <div class="six columns">
                                <div class="greennature-item greennature-content-item">
                                    <p><img class="alignnone size-medium wp-image-5908"
                                        style="padding-left: 27px;" src="{{ asset('img/calculonat.jpg') }}" alt="big-icon" /></p>
                                </div>
                            </div>
                            <div class="six columns">
                                <div class="greennature-item greennature-content-item" style="margin-bottom: 30px;"></div>
                                <div class="greennature-item greennature-icon-with-list-item">
                                    <div class="list-with-icon-ux greennature-ux">
                                        <div class="list-with-icon greennature-left">

                                            <div class="list-with-icon-content">
                                                <div class="list-with-icon-title greennature-skin-title">Sem Uorper Egestas</div>
                                                <div class="list-with-icon-caption">
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                                                        ut labore et dolore magna aliqua. Diam ut venenatis tellus in metus vulputate. Aliquam
                                                        faucibus purus in massa tempor nec feugiat nisl pretium. Ac ut consequat semper viverra.
                                                        Ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat.
                                                    </p>

                                                    <p>Maecenas pharetra convallis posuere morbi leo urna molestie. Ornare arcu dui vivamus arcu felis.
                                                        Bibendum est ultricies integer quis auctor elit sed. Neque vitae tempus quam pellentesque nec nam
                                                        aliquam. Vitae congue eu consequat ac felis donec et odio. Ullamcorper eget nulla facilisi etiam
                                                        dignissim. Lorem ipsum dolor sit amet consectetur adipiscing. Pharetra massa massa ultricies mi.
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="clear"></div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>
                <section id="content-section-5">
                    <div class="greennature-parallax-wrapper greennature-background-image gdlr-show-all greennature-skin-dark-skin" id="greennature-parallax-wrapper-2" data-bgspeed="0.1" style="background-image: url('{{'img/service-bg-2.jpg'}}'); padding-top: 125px; padding-bottom: 90px; ">
                        <div class="container">
                            <div class="greennature-stunning-item-ux greennature-ux">
                                <div class="greennature-item greennature-stunning-item greennature-stunning-center">
                                    <h2 class="stunning-item-title">Praesent commodo cursus magna, vel scelerisque nislet.</h2>
                                    <div class="stunning-item-caption greennature-skin-content">
                                        <p>Praesent commodo cursus magna, vel scelerisque nislet.</p>
                                    </div>
                                        <a class="stunning-item-button greennature-button large greennature-lb-payment" href="#" style="background-color: #fec428; color: #ffffff;">Fazer Comparativo</a>
                            </div>
                            <div class="clear"></div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="clear"></div>
                </section>
            </div>
            <!-- Below Sidebar Section-->

        </div>
        <!-- greennature-content -->
        <div class="clear"></div>
    </div>
    <!-- content wrapper -->


    <div class="greennature-payment-lightbox-overlay" id="greennature-payment-lightbox-overlay"></div>
    <div class="greennature-payment-lightbox-container" id="greennature-payment-lightbox-container">
        <div class="greennature-payment-lightbox-inner">
            <form class="greennature-payment-form" id="greennature-payment-form" >
                {{-- <h3 class="greennature-payment-lightbox-title">
            <span class="greennature-head">You are donating to :</span>
            <span class="greennature-tail">Greennature Foundation</span> --}}
        </h3>

                <div class="greennature-payment-amount">
                    <div class="greennature-payment-amount-head">Insira seus dados abaixo e aguarde nosso contato pelo e-mail ou telefone informados.</div>

                </div>

                <div class="greennature-form-fields">
                    <div class="twelve columns">
                        <div class="columns-wrap greennature-left no-margin">
                            <span class="greennature-head">Nome completo *</span>
                            <input class="greennature-require" type="text" name="name">
                        </div>
                    </div>

                    <div class="clear"></div>
                    <div class="six columns">
                        <div class="columns-wrap greennature-left">
                            <span class="greennature-head">E-mail *</span>
                            <input class="greennature-require greennature-email" type="text" name="email">
                        </div>
                    </div>
                    <div class="six columns">
                        <div class="columns-wrap greennature-right">
                            <span class="greennature-head">Telefone</span>
                            <input type="text" name="phone">
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div class="six columns">
                        <div class="columns-wrap greennature-left">
                            <span class="greennature-head">Variável 1</span>
                            <input class="greennature-require" type="text" name="name">
                        </div>
                    </div>
                    <div class="six columns">
                        <div class="columns-wrap greennature-left">
                            <span class="greennature-head">Variável 2</span>
                            <input class="greennature-require" type="text" name="name">
                        </div>
                    </div>
                    <div class="six columns">
                        <div class="columns-wrap greennature-left">
                            <span class="greennature-head">Variável 3</span>
                            <input class="greennature-require" type="text" name="name">
                        </div>
                    </div>
                    <div class="six columns">
                        <div class="columns-wrap greennature-left">
                            <span class="greennature-head">Variável 4</span>
                            <input class="greennature-require" type="text" name="name">
                        </div>
                    </div>
                    <div class="clear"></div>
                </div>

                <div class="greennature-payment-method">
                <input type="submit" value="Fazer Comparativo" />
            </form>
        </div>
    </div>
</div>

@endsection
