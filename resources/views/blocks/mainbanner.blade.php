<section id="content-section-1">
    <div class="mainbanner greennature-parallax-wrapper greennature-background-image gdlr-show-all greennature-skin-dark-skin" id="greennature-parallax-wrapper-1" data-bgspeed="0"
        style="background-image: url('{{ asset('img/mainbanner/mainbanner.jpg') }}'); padding-top: 40px; padding-bottom: 60px; ">
        <div class="container">
            <div class="greennature-title-item">
                <div class="greennature-item-title-wrapper greennature-item  greennature-left greennature-extra-large ">
                    <div class="greennature-item-title-container container">
                        <div class="greennature-item-title-head">
                            <h3 class="greennature-item-title greennature-skin-title greennature-skin-border mainbanner-title">@yield('page-title')</h3>
                            <div class="greennature-item-title-caption greennature-skin-info mainbanner-subtitle">@yield('page-subtitle')</div>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
</section>
