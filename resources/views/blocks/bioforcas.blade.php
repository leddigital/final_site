<section id="content-section-5">
    <div class="greennature-parallax-wrapper greennature-background-image gdlr-show-all greennature-skin-dark-skin" id="greennature-parallax-wrapper-2" data-bgspeed="0.1" style="background-image: url('img/service-bg-2.jpg'); padding-top: 125px; padding-bottom: 90px; ">
        <div class="container">
            <div class="greennature-stunning-item-ux greennature-ux">
                <div class="greennature-item greennature-stunning-item greennature-stunning-center">
                    <h2 class="stunning-item-title">Lorem Ipsum Dolor Sit Amet</h2>
                    <div class="stunning-item-caption greennature-skin-content">
                        <p>Praesent commodo cursus magna, vel scelerisque nislet.</p>
                    </div>
                <a class="stunning-item-button greennature-button large"
                    href="{{ route('nav.bioforcas') }}" style="background-color: #fec428; color: #ffffff;">
                        Conheça nossos produtos
                    </a>
                    {{-- <a class="stunning-item-button greennature-button large" href="#">Act Now!</a> --}}
                </div>
            </div>
            <div class="clear"></div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="clear"></div>
</section>
