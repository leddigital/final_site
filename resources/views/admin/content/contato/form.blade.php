@extends('layouts.admin')
@section('title', 'Produto')
@section('content')

<header class="page-header">
    <h2>Contatos</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Contato</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action="{{ isset($entity->id)?route('contato.edit.save',['id'=>$entity->id]):route('contato.save') }}"
            data-reload="{{ route('contato.index') }}">
            @csrf
            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Contatos</h2>
                    <p class="card-subtitle">
                        Cadastro de revendedor
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-8">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                            </span>
                                        </span>
                                        <input type="text" value="{{ (isset($entity->name) && $entity->name != "")?$entity->name:"" }}" name="name" class="form-control"
                                            placeholder="Nome completo">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-align-right" aria-hidden="true"></i>
                                            </span>
                                        </span>
                                        <input type="text" value="{{ (isset($entity->role) && $entity->role != "")?$entity->role:"" }}" name="role" class="form-control"
                                            placeholder="Nome completo">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-5">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                            </span>
                                        </span>
                                        <input type="email" value="{{ (isset($entity->email) && $entity->email != "")?$entity->email:"" }}" name="email" class="form-control"
                                            placeholder="Nome completo">
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-phone" aria-hidden="true"></i>
                                            </span>
                                        </span>
                                        <input type="text" value="{{ (isset($entity->contact) && $entity->contact != "")?$entity->contact:"" }}" name="contact" class="form-control"
                                            placeholder="Nome completo">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fas fa-map-marker-alt" aria-hidden="true"></i>
                                            </span>
                                        </span>
                                        <input type="text" value="{{ (isset($entity->address) && $entity->address != "")?$entity->address:"" }}" name="address" class="form-control"
                                            placeholder="Nome completo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            <section class="card">
                <div class="card-body" style="display: block;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                        Salvar</button>
                </div>
            </section>

        </form>
    </div>
</div>



<script id="delete_gallery_template" type="x-tmpl-mustache">
    <input type="text" name="delete_gallery[]" class="form-control input-sm" placeholder="" value=" "/>
</script>
<script id="thumb_template" type="x-tmpl-mustache">
    <div class="gallery_item">
        <img src="@{{ thumb }}" class="img-responsive"/>
        <input type="hidden" name="gallery_image[]" value="@{{ original }}" />
        <input type="hidden" name="gallery_image_thumb[]" value="@{{ thumb }}" />
        <input type="text" name="gallery_image_description[]" class="form-control input-sm" placeholder="Descrição da imagem"/>
        <a href="javascript:;" class="" id="gallery_item_remove">
            <i class="mdi mdi-delete"></i>Remover
        </a>
    </div>
</script>

@endsection
