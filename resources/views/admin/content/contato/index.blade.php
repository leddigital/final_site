@extends('layouts.admin')
@section('title', 'Produtos')
@section('content')
<header class="page-header">
    <h2>Produtos</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Contatos</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <section class="card">
            <div class="card-body">
                <a class="mb-1 mt-1 mr-1 btn btn-success" href="{{ route('contato.form') }}">
                    <i class="fas fa-plus"></i> Cadastrar Novo
                </a>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">

                <h2 class="card-title">Contatos Cadastrados</h2>
                <p class="card-subtitle">
                    Listagem de todas os contatos cadastrados.
                </p>
            </header>
            <div class="card-body">
                <table class="table table-bordered table-striped mb-0"
                id="datatable-default" data-delete="{{ route('contato.delete') }}"
                data-edit="{{ route('contato.edit') }}"
                data-list="{{ route('contato.list.all') }}" data-cols='[
                    { "data": "name","title":"Nome" },
                    { "data": "role","title":"Cargo" },
                    { "data": "email","title":"E-mail" },
                    { "data": "contact","title":"Contato" },
                    { "data": "address","title":"Cidade / Estado" },
                    { "title":"Opções" }
                ]'>
                    <thead>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
@endsection
