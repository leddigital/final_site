@extends('layouts.admin')
@section('title', 'Produtos')
@section('content')

<header class="page-header">
    <h2>Bionews</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Bionews</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <section class="card">
            <div class="card-body">
                <a class="mb-1 mt-1 mr-1 btn btn-success" href="{{ route('posts.form') }}">
                    <i class="fas fa-plus"></i> Nova Notícia
                </a>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col">
        <section class="card">
            <header class="card-header">

                <h2 class="card-title">Notícias publicadas</h2>
                <p class="card-subtitle">
                    Listagem de todas as notícias cadastradas.
                </p>
            </header>
            <div class="card-body">
                <table class="table table-bordered table-striped mb-0"
                id="datatable-default" data-delete="{{ route('posts.delete') }}"
                data-edit="{{ route('posts.edit') }}"
                data-list="{{ route('posts.list.all') }}" data-cols='[
                    { "data": "title","title":"Título" },
                    { "data": "created_at","title":"Data de publicação" },
                    { "title":"Opções" }
                ]'>
                    <thead>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </section>
    </div>
</div>
@endsection
