@extends('layouts.admin')
@section('title', 'Produto')
@section('content')

<header class="page-header">
    <h2>Bionews</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Bionews</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action="{{ isset($entity->id)?route('posts.edit.save',['id'=>$entity->id]):route('posts.save') }}"
            data-reload="{{ route('posts.index') }}">
            @csrf

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Título</h2>
                    <p class="card-subtitle">
                        Cadastro de notícia
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <span class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="fa fa-align-right" aria-hidden="true"></i>
                                            </span>
                                        </span>
                                        <input type="text"
                                            value="{{ (isset($entity->title) && $entity->title != "")?$entity->title:"" }}"
                                            name="title" class="form-control" placeholder="Tìtulo">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Descrição</h2>
                    <p class="card-subtitle">
                        Escreve um pequeno resumo da notícia
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <textarea name="short_description" rows="3" class="form-control">{{ (isset($entity->short_description) && $entity->short_description != "")?$entity->short_description:"" }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Conteúdo</h2>
                    <p class="card-subtitle">
                        Preenchar as informações abaixo para exibir os textos dinâmicos de acordo com o banner.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <textarea name="content"
                                class="summernote">{{ isset($entity->content)?$entity->content:'' }}</textarea>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Miniatura</h2>
                    <p class="card-subtitle">
                        Selecione uma imagem para miniatura.<br />
                        <br />
                        <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição
                            do campo.</strong>
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <small>Tamanho da imagem 400px x 300px</small>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fas fa-file fileupload-exists"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Trocar</span>
                                        <span class="fileupload-new">Selecionar Imagem</span>
                                        <input type="file" name='banner' accept="image/*"
                                            onchange='loadPreviewMiniatura(this, 400,300)' {{ isset($entity)?'':'required' }}
                                            value="{{ isset($entity->image)!=""?$entity->image:'' }}">
                                    </span>
                                    <a href="javascript:;" class="btn btn-default fileupload-exists"
                                        data-dismiss="fileupload">Remove</a>
                                    @if (isset($entity->image)!="")
                                    <a href="javascript:;" class="btn btn-default" id="remove_image_default">Remove</a>
                                    @endif
                                    <input type="hidden" name="remove_image_default" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <textarea id="base64_min" name="base64_min" style="display:none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="img-content">
                                <img id='output_min' class='img-fluid'
                                    src='{{ isset($entity->image)!=""?"/img/bionews/".$entity->feat_image:'' }}'>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Imagem Principal</h2>
                    <p class="card-subtitle">
                        Selecione uma imagem como imagem principal.<br />
                        <br />
                        <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição
                            do campo.</strong>
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <small>Tamanho da imagem 750px x 430px</small>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fas fa-file fileupload-exists"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Trocar</span>
                                        <span class="fileupload-new">Selecionar Imagem</span>
                                        <input type="file" name='banner' accept="image/*"
                                            onchange='loadPreview(this, 750,430)' {{ isset($entity)?'':'required' }}
                                            value="{{ isset($entity->image)!=""?"/content/".$entity->id."/".$entity->image:'' }}">
                                    </span>
                                    <a href="javascript:;" class="btn btn-default fileupload-exists"
                                        data-dismiss="fileupload">Remove</a>
                                    @if (isset($entity->image)!="")
                                    <a href="javascript:;" class="btn btn-default" id="remove_image_default">Remove</a>
                                    @endif
                                    <input type="hidden" name="remove_image_default" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <textarea id="base64" name="base64" style="display:none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="img-content">
                                <img id='output' class='img-fluid'
                                    src='{{ isset($entity->image)!=""?"/img/bionews/".$entity->image:'' }}'>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <div class="card-body" style="display: block;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                        Salvar</button>
                </div>
            </section>

        </form>
    </div>
</div>



<script id="delete_gallery_template" type="x-tmpl-mustache">
    <input type="text" name="delete_gallery[]" class="form-control input-sm" placeholder="" value=" "/>
</script>
<script id="thumb_template" type="x-tmpl-mustache">
    <div class="gallery_item">
        <img src="@{{ thumb }}" class="img-responsive"/>
        <input type="hidden" name="gallery_image[]" value="@{{ original }}" />
        <input type="hidden" name="gallery_image_thumb[]" value="@{{ thumb }}" />
        <input type="text" name="gallery_image_description[]" class="form-control input-sm" placeholder="Descrição da imagem"/>
        <a href="javascript:;" class="" id="gallery_item_remove">
            <i class="mdi mdi-delete"></i>Remover
        </a>
    </div>
</script>

@endsection
