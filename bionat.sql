-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: bionat
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `text_1` varchar(255) DEFAULT NULL,
  `font_color_1` varchar(255) DEFAULT NULL,
  `font_weight_1` varchar(255) DEFAULT NULL,
  `text_2` varchar(255) DEFAULT NULL,
  `font_color_2` varchar(255) DEFAULT NULL,
  `font_weight_2` varchar(255) DEFAULT NULL,
  `button_link` varchar(255) DEFAULT NULL,
  `button_text` varchar(255) DEFAULT NULL,
  `button_bg_color` varchar(255) DEFAULT NULL,
  `text_align` varchar(255) DEFAULT NULL,
  `button_text_color` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,'banner_5df77cb2f12fa.jpeg','Banner 1','2019-12-16 12:46:42','2019-12-16 12:46:42',NULL,NULL,NULL,'100',NULL,NULL,'100',NULL,NULL,NULL,'ml',NULL),(2,'banner_5df77cc17b074.jpeg','Banner 2','2019-12-16 12:46:57','2019-12-17 12:20:59',NULL,NULL,NULL,'100',NULL,NULL,'100',NULL,NULL,NULL,'ml',NULL),(3,'banner_5df77cd0d3376.jpeg','Banner 3','2019-12-16 12:47:12','2019-12-16 12:47:12',NULL,NULL,NULL,'100',NULL,NULL,'100',NULL,NULL,NULL,'ml',NULL);
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Qualquer coisa','qualquer-coisa','news'),(2,'Bio Ciências','bio-ciencias','news');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_has_contents`
--

DROP TABLE IF EXISTS `categories_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories_has_contents` (
  `categories_id` int(11) NOT NULL,
  `contents_id` int(11) NOT NULL,
  PRIMARY KEY (`categories_id`,`contents_id`),
  KEY `fk_categories_has_contents_contents1_idx` (`contents_id`),
  KEY `fk_categories_has_contents_categories1_idx` (`categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_has_contents`
--

LOCK TABLES `categories_has_contents` WRITE;
/*!40000 ALTER TABLE `categories_has_contents` DISABLE KEYS */;
INSERT INTO `categories_has_contents` VALUES (1,1),(2,1),(1,2);
/*!40000 ALTER TABLE `categories_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT 'null',
  `role` varchar(255) NOT NULL DEFAULT 'null',
  `email` varchar(255) NOT NULL DEFAULT 'null',
  `contact` varchar(255) NOT NULL DEFAULT 'null',
  `address` varchar(255) NOT NULL DEFAULT 'null',
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (7,'Guedes Ferreira do Prado','Coordenador Regional de Vendas','guedes.prado@bionatagro.com','(66) 99951-8267','Sorriso/MT','2019-12-17 13:08:54','2019-12-17 13:08:54'),(8,'Diego Ramos Bicudo','Coordenador Regional de Vendas','diego.bicudo@bionatagro.com','34 99912-8303','Uberlândia/MG','2019-12-17 13:09:12','2019-12-17 13:09:12'),(9,'Sandro Moreira Marcantes','Consultor Técnico Comercial','sandro.marcantes@bionatagro.com.br','43 99146-4867','Ponta Grossa/PR','2019-12-17 13:09:30','2019-12-17 13:09:30'),(10,'Helena Mendes de Freitas','Consultora Técnica Comercial','helena.freitas@bionatagro.com','66 99725-0558','Sinop/MT','2019-12-17 13:09:49','2019-12-17 13:09:49'),(11,'Galvão Aleixo','Consultor Técnico Comercial','galvao.aleixo@bionatagro.com','66 99641-3036','Campo Novo do Parecis/MT','2019-12-17 13:10:05','2019-12-17 13:10:05'),(12,'Sandro Moreira Marcantes','Consultor Técnico Comercial','sandro.marcantes@bionatagro.com.br','43 99146-4867','Olímpia/SP','2019-12-17 13:10:32','2019-12-17 13:10:32');
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents`
--

DROP TABLE IF EXISTS `contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `content` longtext,
  `type` varchar(45) NOT NULL COMMENT 'post,news,page,page-name',
  `url` varchar(45) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `categoria` varchar(255) DEFAULT 'null',
  `embalagem` varchar(255) DEFAULT 'null',
  `composicao` varchar(255) DEFAULT 'null',
  `recomendacao` varchar(255) DEFAULT 'null',
  `bula` varchar(255) DEFAULT 'null',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_UNIQUE` (`url`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents`
--

LOCK TABLES `contents` WRITE;
/*!40000 ALTER TABLE `contents` DISABLE KEYS */;
INSERT INTO `contents` VALUES (1,'Bovenat','É a linha de inseticidas microbianos à base de isolados do gênero Beauveria, microrganismos generalistas que apresentam alta eficiência, capacidade de penetração e colonização de insetos-pragas.','<div><b>Recomendação</b>:&nbsp;&nbsp;</div><div>Indicado para aplicação foliar para o controle da mosca branca (Bemisia tabaci raça B),cigarrinha do milho (Dalbulus maidis), ácaro rajado (Tetranychus urticae) e aplicação via iscas para Cosmopolites sordidus.</div><div><br></div><div><b>Composição:&nbsp;&nbsp;</b></div><div>Beauveria bassiana cepa IBCB 66 - 1,0 x 1010 UFC/g 110g/Kg(11,0%)</div><div>Outros ingredientes - 890g/Kg (89,0%)</div><div><br></div><div><b>Cultura Recomendada:</b></div><div>O produto apresenta eficiência agronômica comprovada nas culturas da soja, pepino, banana, morango, milho e pode ser utilizado em qualquer outra cultura com ocorrência dos alvos biológicos.</div><div><br></div><div><b>Quando utilizar:&nbsp;&nbsp;</b></div><div>Em todas as culturas com ocorrência do alvo biológico.</div><div><br></div><div><b>Como funciona:&nbsp;&nbsp;</b></div><div>O fungo atua sobre diferentes estágios de desenvolvimento dos hospedeiros, como larvas, pupas e adultos. A infecção ocorre normalmente via tegumento, onde o fungo coloniza totalmente o inseto decorridas 72 horas, levando-o à morte. Os insetos atacados apresentam-se cobertos por micélio branco que esporula em condições de temperatura de 23 a 30ºC e umidade relativa acima de 60%.</div><div><br></div><div><b>Como aplicar:&nbsp;&nbsp;</b></div><div>- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida e bactericidas) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do produto.</div><div>-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</div><div>-Para a aplicação pode-se utilizar pulverizador costal tratorizado, ou aérea.</div><div>-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</div><div>-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</div><div>-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre a deriva e perdas do produto por evaporação.</div><div><br></div><div>- lscas: Diluir 500g do produto em 5 litros de agua ou óleo vegetal formando uma pasta homogênea. Aplicar a pasta sobre toda a superfície seccionada de iscas-atrativas (tipo telha ou queijo). Distribuir na base das plantas 100 iscas/ha, mantendo-as com a superfície tratada voltada para o solo. Substituir as iscas em intervalos de 15 dias, observando o limite máximo de 3 aplicações.</div><div><br></div><div><b>Armazenamento:</b></div><div>- Mantenha o produto em sua embalagem origi­nal, sempre fechada.</div><div>- O local deve ser exclusivo para produtos tóxi­cos, devendo ser isolado de produtos químicos, alimentos, bebidas, rações ou outros materiais.</div><div>- A construção deve ser de alvenaria ou de mate­rial não combustível.</div><div>- O local deve ser ventilado, coberto e ter piso impermeável.</div><div>- Tranque o local, evitando o acesso de pessoas não autorizadas, principalmente crianças.</div><div>- Deve haver sempre embalagens adequadas dis­poníveis para envolver embalagens rompidas ou para o recolhimento de produtos vazados.</div><div>- Em caso de armazéns, deverão ser seguidas as instruções constantes da NBR 9843, da Associa­ção Brasileira de Normas Técnicas - ABNT.</div><div>- Observe as disposições constantes da legisla­ção estadual e municipal.</div><div>- Armazenar sob refrigeração a 5°C ou -15°C por até 120 dias, e a 27°C por até 90 dias.</div><div><br></div><div><b>Transporte:</b></div><div>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.</div>','bioforca','bovenat','bovenat.png','2019-12-18 15:54:09','2019-12-18 15:54:09','Bioinseticida','Saco aluminizado sanfonado com capacidade de 1 Kg','Beauveria bassiana','Controle da mosca branca','Saco aluminizado sanfonado com capacidade de 1 Kg'),(2,'Metarhizonat','É a linha de inseticidas microbianos à base do fungo Metarhizium, microrganismos que infectam e matam uma ampla gama de espécies de pragas, principalmente os insetos que vivem próximo ao solo.','<div><b>Recomendação</b>:&nbsp;&nbsp;</div><div>Indicado para aplicação foliar para o controle da mosca branca (Bemisia tabaci raça B),cigarrinha do milho (Dalbulus maidis), ácaro rajado (Tetranychus urticae) e aplicação via iscas para Cosmopolites sordidus.</div><div><br></div><div><b>Composição:&nbsp;&nbsp;</b></div><div>Beauveria bassiana cepa IBCB 66 - 1,0 x 1010 UFC/g 110g/Kg(11,0%)</div><div>Outros ingredientes - 890g/Kg (89,0%)</div><div><br></div><div><b>Cultura Recomendada:</b></div><div>O produto apresenta eficiência agronômica comprovada nas culturas da soja, pepino, banana, morango, milho e pode ser utilizado em qualquer outra cultura com ocorrência dos alvos biológicos.</div><div><br></div><div><b>Quando utilizar:&nbsp;&nbsp;</b></div><div>Em todas as culturas com ocorrência do alvo biológico.</div><div><br></div><div><b>Como funciona:&nbsp;&nbsp;</b></div><div>O fungo atua sobre diferentes estágios de desenvolvimento dos hospedeiros, como larvas, pupas e adultos. A infecção ocorre normalmente via tegumento, onde o fungo coloniza totalmente o inseto decorridas 72 horas, levando-o à morte. Os insetos atacados apresentam-se cobertos por micélio branco que esporula em condições de temperatura de 23 a 30ºC e umidade relativa acima de 60%.</div><div><br></div><div><b>Como aplicar:&nbsp;&nbsp;</b></div><div>- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida e bactericidas) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do produto.</div><div>-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</div><div>-Para a aplicação pode-se utilizar pulverizador costal tratorizado, ou aérea.</div><div>-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</div><div>-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</div><div>-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre a deriva e perdas do produto por evaporação.</div><div><br></div><div>- lscas: Diluir 500g do produto em 5 litros de agua ou óleo vegetal formando uma pasta homogênea. Aplicar a pasta sobre toda a superfície seccionada de iscas-atrativas (tipo telha ou queijo). Distribuir na base das plantas 100 iscas/ha, mantendo-as com a superfície tratada voltada para o solo. Substituir as iscas em intervalos de 15 dias, observando o limite máximo de 3 aplicações.</div><div><br></div><div><b>Armazenamento:</b></div><div>- Mantenha o produto em sua embalagem origi­nal, sempre fechada.</div><div>- O local deve ser exclusivo para produtos tóxi­cos, devendo ser isolado de produtos químicos, alimentos, bebidas, rações ou outros materiais.</div><div>- A construção deve ser de alvenaria ou de mate­rial não combustível.</div><div>- O local deve ser ventilado, coberto e ter piso impermeável.</div><div>- Tranque o local, evitando o acesso de pessoas não autorizadas, principalmente crianças.</div><div>- Deve haver sempre embalagens adequadas dis­poníveis para envolver embalagens rompidas ou para o recolhimento de produtos vazados.</div><div>- Em caso de armazéns, deverão ser seguidas as instruções constantes da NBR 9843, da Associa­ção Brasileira de Normas Técnicas - ABNT.</div><div>- Observe as disposições constantes da legisla­ção estadual e municipal.</div><div>- Armazenar sob refrigeração a 5°C ou -15°C por até 120 dias, e a 27°C por até 90 dias.</div><div><br></div><div><b>Transporte:</b></div><div>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.</div>','bioforca','metarhizonat','metarhizonat.png','2019-12-18 15:54:09','2019-12-18 15:54:09','Bioinseticida','Saco aluminizado sanfonado com capacidade de 1 Kg','Metarhizium','Controle da mosca branca','Saco aluminizado sanfonado com capacidade de 1 Kg'),(3,'Bacilonat','É a linha de produtos que possui na composição, uma ou mais espécie(s) de bactéria(s) do gênero Bacillus, microrganismos com ação inseticida, fungicida, bactericida ou nematicida.','<div><b>Recomendação</b>:&nbsp;&nbsp;</div><div>Indicado para aplicação foliar para o controle da mosca branca (Bemisia tabaci raça B),cigarrinha do milho (Dalbulus maidis), ácaro rajado (Tetranychus urticae) e aplicação via iscas para Cosmopolites sordidus.</div><div><br></div><div><b>Composição:&nbsp;&nbsp;</b></div><div>Beauveria bassiana cepa IBCB 66 - 1,0 x 1010 UFC/g 110g/Kg(11,0%)</div><div>Outros ingredientes - 890g/Kg (89,0%)</div><div><br></div><div><b>Cultura Recomendada:</b></div><div>O produto apresenta eficiência agronômica comprovada nas culturas da soja, pepino, banana, morango, milho e pode ser utilizado em qualquer outra cultura com ocorrência dos alvos biológicos.</div><div><br></div><div><b>Quando utilizar:&nbsp;&nbsp;</b></div><div>Em todas as culturas com ocorrência do alvo biológico.</div><div><br></div><div><b>Como funciona:&nbsp;&nbsp;</b></div><div>O fungo atua sobre diferentes estágios de desenvolvimento dos hospedeiros, como larvas, pupas e adultos. A infecção ocorre normalmente via tegumento, onde o fungo coloniza totalmente o inseto decorridas 72 horas, levando-o à morte. Os insetos atacados apresentam-se cobertos por micélio branco que esporula em condições de temperatura de 23 a 30ºC e umidade relativa acima de 60%.</div><div><br></div><div><b>Como aplicar:&nbsp;&nbsp;</b></div><div>- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida e bactericidas) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do produto.</div><div>-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</div><div>-Para a aplicação pode-se utilizar pulverizador costal tratorizado, ou aérea.</div><div>-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</div><div>-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</div><div>-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre a deriva e perdas do produto por evaporação.</div><div><br></div><div>- lscas: Diluir 500g do produto em 5 litros de agua ou óleo vegetal formando uma pasta homogênea. Aplicar a pasta sobre toda a superfície seccionada de iscas-atrativas (tipo telha ou queijo). Distribuir na base das plantas 100 iscas/ha, mantendo-as com a superfície tratada voltada para o solo. Substituir as iscas em intervalos de 15 dias, observando o limite máximo de 3 aplicações.</div><div><br></div><div><b>Armazenamento:</b></div><div>- Mantenha o produto em sua embalagem origi­nal, sempre fechada.</div><div>- O local deve ser exclusivo para produtos tóxi­cos, devendo ser isolado de produtos químicos, alimentos, bebidas, rações ou outros materiais.</div><div>- A construção deve ser de alvenaria ou de mate­rial não combustível.</div><div>- O local deve ser ventilado, coberto e ter piso impermeável.</div><div>- Tranque o local, evitando o acesso de pessoas não autorizadas, principalmente crianças.</div><div>- Deve haver sempre embalagens adequadas dis­poníveis para envolver embalagens rompidas ou para o recolhimento de produtos vazados.</div><div>- Em caso de armazéns, deverão ser seguidas as instruções constantes da NBR 9843, da Associa­ção Brasileira de Normas Técnicas - ABNT.</div><div>- Observe as disposições constantes da legisla­ção estadual e municipal.</div><div>- Armazenar sob refrigeração a 5°C ou -15°C por até 120 dias, e a 27°C por até 90 dias.</div><div><br></div><div><b>Transporte:</b></div><div>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.</div>','bioforca','bacilonat','bacilonat.png','2019-12-18 15:54:09','2019-12-18 15:54:09','Bioinseticida','Saco aluminizado sanfonado com capacidade de 1 Kg','Beauveria bassiana','Controle da mosca branca','Saco aluminizado sanfonado com capacidade de 1 Kg'),(4,'Baculonat','É a linha de inseticidas microbianos à base de Baculovírus, microrganismos altamente específicos para o controle de Lepidópteros, que quando ingerido por lagartas, provocam a infecção e, consequentemente, a morte. ','<div><b>Recomendação</b>:&nbsp;&nbsp;</div><div>Indicado para aplicação foliar para o controle da mosca branca (Bemisia tabaci raça B),cigarrinha do milho (Dalbulus maidis), ácaro rajado (Tetranychus urticae) e aplicação via iscas para Cosmopolites sordidus.</div><div><br></div><div><b>Composição:&nbsp;&nbsp;</b></div><div>Beauveria bassiana cepa IBCB 66 - 1,0 x 1010 UFC/g 110g/Kg(11,0%)</div><div>Outros ingredientes - 890g/Kg (89,0%)</div><div><br></div><div><b>Cultura Recomendada:</b></div><div>O produto apresenta eficiência agronômica comprovada nas culturas da soja, pepino, banana, morango, milho e pode ser utilizado em qualquer outra cultura com ocorrência dos alvos biológicos.</div><div><br></div><div><b>Quando utilizar:&nbsp;&nbsp;</b></div><div>Em todas as culturas com ocorrência do alvo biológico.</div><div><br></div><div><b>Como funciona:&nbsp;&nbsp;</b></div><div>O fungo atua sobre diferentes estágios de desenvolvimento dos hospedeiros, como larvas, pupas e adultos. A infecção ocorre normalmente via tegumento, onde o fungo coloniza totalmente o inseto decorridas 72 horas, levando-o à morte. Os insetos atacados apresentam-se cobertos por micélio branco que esporula em condições de temperatura de 23 a 30ºC e umidade relativa acima de 60%.</div><div><br></div><div><b>Como aplicar:&nbsp;&nbsp;</b></div><div>- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida e bactericidas) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do produto.</div><div>-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</div><div>-Para a aplicação pode-se utilizar pulverizador costal tratorizado, ou aérea.</div><div>-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</div><div>-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</div><div>-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre a deriva e perdas do produto por evaporação.</div><div><br></div><div>- lscas: Diluir 500g do produto em 5 litros de agua ou óleo vegetal formando uma pasta homogênea. Aplicar a pasta sobre toda a superfície seccionada de iscas-atrativas (tipo telha ou queijo). Distribuir na base das plantas 100 iscas/ha, mantendo-as com a superfície tratada voltada para o solo. Substituir as iscas em intervalos de 15 dias, observando o limite máximo de 3 aplicações.</div><div><br></div><div><b>Armazenamento:</b></div><div>- Mantenha o produto em sua embalagem origi­nal, sempre fechada.</div><div>- O local deve ser exclusivo para produtos tóxi­cos, devendo ser isolado de produtos químicos, alimentos, bebidas, rações ou outros materiais.</div><div>- A construção deve ser de alvenaria ou de mate­rial não combustível.</div><div>- O local deve ser ventilado, coberto e ter piso impermeável.</div><div>- Tranque o local, evitando o acesso de pessoas não autorizadas, principalmente crianças.</div><div>- Deve haver sempre embalagens adequadas dis­poníveis para envolver embalagens rompidas ou para o recolhimento de produtos vazados.</div><div>- Em caso de armazéns, deverão ser seguidas as instruções constantes da NBR 9843, da Associa­ção Brasileira de Normas Técnicas - ABNT.</div><div>- Observe as disposições constantes da legisla­ção estadual e municipal.</div><div>- Armazenar sob refrigeração a 5°C ou -15°C por até 120 dias, e a 27°C por até 90 dias.</div><div><br></div><div><b>Transporte:</b></div><div>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.</div>','bioforca','baculonat','baculonat.png','2019-12-18 15:54:09','2019-12-18 15:54:09','Bioinseticida','Saco aluminizado sanfonado com capacidade de 1 Kg','Baculovírus','Controle da mosca branca','Saco aluminizado sanfonado com capacidade de 1 Kg'),(5,'Solunat','É a linha de produtos à base de bactérias que fixam nitrogênio, solubilizam nutrientes e/ou promovem o crescimento das plantas. ','<div><b>Recomendação</b>:&nbsp;&nbsp;</div><div>Indicado para aplicação foliar para o controle da mosca branca (Bemisia tabaci raça B),cigarrinha do milho (Dalbulus maidis), ácaro rajado (Tetranychus urticae) e aplicação via iscas para Cosmopolites sordidus.</div><div><br></div><div><b>Composição:&nbsp;&nbsp;</b></div><div>Beauveria bassiana cepa IBCB 66 - 1,0 x 1010 UFC/g 110g/Kg(11,0%)</div><div>Outros ingredientes - 890g/Kg (89,0%)</div><div><br></div><div><b>Cultura Recomendada:</b></div><div>O produto apresenta eficiência agronômica comprovada nas culturas da soja, pepino, banana, morango, milho e pode ser utilizado em qualquer outra cultura com ocorrência dos alvos biológicos.</div><div><br></div><div><b>Quando utilizar:&nbsp;&nbsp;</b></div><div>Em todas as culturas com ocorrência do alvo biológico.</div><div><br></div><div><b>Como funciona:&nbsp;&nbsp;</b></div><div>O fungo atua sobre diferentes estágios de desenvolvimento dos hospedeiros, como larvas, pupas e adultos. A infecção ocorre normalmente via tegumento, onde o fungo coloniza totalmente o inseto decorridas 72 horas, levando-o à morte. Os insetos atacados apresentam-se cobertos por micélio branco que esporula em condições de temperatura de 23 a 30ºC e umidade relativa acima de 60%.</div><div><br></div><div><b>Como aplicar:&nbsp;&nbsp;</b></div><div>- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida e bactericidas) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do produto.</div><div>-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</div><div>-Para a aplicação pode-se utilizar pulverizador costal tratorizado, ou aérea.</div><div>-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</div><div>-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</div><div>-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre a deriva e perdas do produto por evaporação.</div><div><br></div><div>- lscas: Diluir 500g do produto em 5 litros de agua ou óleo vegetal formando uma pasta homogênea. Aplicar a pasta sobre toda a superfície seccionada de iscas-atrativas (tipo telha ou queijo). Distribuir na base das plantas 100 iscas/ha, mantendo-as com a superfície tratada voltada para o solo. Substituir as iscas em intervalos de 15 dias, observando o limite máximo de 3 aplicações.</div><div><br></div><div><b>Armazenamento:</b></div><div>- Mantenha o produto em sua embalagem origi­nal, sempre fechada.</div><div>- O local deve ser exclusivo para produtos tóxi­cos, devendo ser isolado de produtos químicos, alimentos, bebidas, rações ou outros materiais.</div><div>- A construção deve ser de alvenaria ou de mate­rial não combustível.</div><div>- O local deve ser ventilado, coberto e ter piso impermeável.</div><div>- Tranque o local, evitando o acesso de pessoas não autorizadas, principalmente crianças.</div><div>- Deve haver sempre embalagens adequadas dis­poníveis para envolver embalagens rompidas ou para o recolhimento de produtos vazados.</div><div>- Em caso de armazéns, deverão ser seguidas as instruções constantes da NBR 9843, da Associa­ção Brasileira de Normas Técnicas - ABNT.</div><div>- Observe as disposições constantes da legisla­ção estadual e municipal.</div><div>- Armazenar sob refrigeração a 5°C ou -15°C por até 120 dias, e a 27°C por até 90 dias.</div><div><br></div><div><b>Transporte:</b></div><div>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.</div>','bioforca','solunat','solunat.png','2019-12-18 15:54:09','2019-12-18 15:54:09','Bioinseticida','Saco aluminizado sanfonado com capacidade de 1 Kg','Metarhizium','Controle da mosca branca','Saco aluminizado sanfonado com capacidade de 1 Kg'),(6,'ADBio','É a linha de produtos que atua como potencializador da ação dos produtos biológicos. Os produtos reduzem os efeitos dos fatores adversos sobre os microrganismos, melhoram as características da calda de pulverização, facilitando a aplicação.','<div><b>Recomendação</b>:&nbsp;&nbsp;</div><div>Indicado para aplicação foliar para o controle da mosca branca (Bemisia tabaci raça B),cigarrinha do milho (Dalbulus maidis), ácaro rajado (Tetranychus urticae) e aplicação via iscas para Cosmopolites sordidus.</div><div><br></div><div><b>Composição:&nbsp;&nbsp;</b></div><div>Beauveria bassiana cepa IBCB 66 - 1,0 x 1010 UFC/g 110g/Kg(11,0%)</div><div>Outros ingredientes - 890g/Kg (89,0%)</div><div><br></div><div><b>Cultura Recomendada:</b></div><div>O produto apresenta eficiência agronômica comprovada nas culturas da soja, pepino, banana, morango, milho e pode ser utilizado em qualquer outra cultura com ocorrência dos alvos biológicos.</div><div><br></div><div><b>Quando utilizar:&nbsp;&nbsp;</b></div><div>Em todas as culturas com ocorrência do alvo biológico.</div><div><br></div><div><b>Como funciona:&nbsp;&nbsp;</b></div><div>O fungo atua sobre diferentes estágios de desenvolvimento dos hospedeiros, como larvas, pupas e adultos. A infecção ocorre normalmente via tegumento, onde o fungo coloniza totalmente o inseto decorridas 72 horas, levando-o à morte. Os insetos atacados apresentam-se cobertos por micélio branco que esporula em condições de temperatura de 23 a 30ºC e umidade relativa acima de 60%.</div><div><br></div><div><b>Como aplicar:&nbsp;&nbsp;</b></div><div>- Antes de realizar o preparo da calda de pulverização certificar da limpeza do pulverizador. Caso o pulverizador apresente resíduos de produtos de aplicações anteriores (principalmente fungicida e bactericidas) é de fundamental importância a limpeza do equipamento, pois pode afetar o desempenho do produto.</div><div>-Recomenda-se que se inicie a aplicação logo após o preparo da calda de pulverização.</div><div>-Para a aplicação pode-se utilizar pulverizador costal tratorizado, ou aérea.</div><div>-Efetuar as aplicações de forma que possibilitem uma boa cobertura da parte aérea das plantas com a presença da praga alvo, sem causar escorrimento. Sendo o volume de calda variável de acordo com a espécie de planta.</div><div>-Recomenda-se aplicar nas horas mais frescas do dia, preferencialmente no final da tarde. Evitar aplicação em condição de temperatura acima de 27°C ou na presença de ventos fortes (velocidade acima de 10 Km/hora), bem como com umidade relativa do ar abaixo de 70% ou com alta intensidade de radiação ultravioleta (UV).</div><div>-A escolha dos equipamentos a serem utilizados para aplicação deste produto poderá sofrer alterações a critério do Engenheiro Agrônomo, tomando-se o cuidado de evitar sempre a deriva e perdas do produto por evaporação.</div><div><br></div><div>- lscas: Diluir 500g do produto em 5 litros de agua ou óleo vegetal formando uma pasta homogênea. Aplicar a pasta sobre toda a superfície seccionada de iscas-atrativas (tipo telha ou queijo). Distribuir na base das plantas 100 iscas/ha, mantendo-as com a superfície tratada voltada para o solo. Substituir as iscas em intervalos de 15 dias, observando o limite máximo de 3 aplicações.</div><div><br></div><div><b>Armazenamento:</b></div><div>- Mantenha o produto em sua embalagem origi­nal, sempre fechada.</div><div>- O local deve ser exclusivo para produtos tóxi­cos, devendo ser isolado de produtos químicos, alimentos, bebidas, rações ou outros materiais.</div><div>- A construção deve ser de alvenaria ou de mate­rial não combustível.</div><div>- O local deve ser ventilado, coberto e ter piso impermeável.</div><div>- Tranque o local, evitando o acesso de pessoas não autorizadas, principalmente crianças.</div><div>- Deve haver sempre embalagens adequadas dis­poníveis para envolver embalagens rompidas ou para o recolhimento de produtos vazados.</div><div>- Em caso de armazéns, deverão ser seguidas as instruções constantes da NBR 9843, da Associa­ção Brasileira de Normas Técnicas - ABNT.</div><div>- Observe as disposições constantes da legisla­ção estadual e municipal.</div><div>- Armazenar sob refrigeração a 5°C ou -15°C por até 120 dias, e a 27°C por até 90 dias.</div><div><br></div><div><b>Transporte:</b></div><div>As embalagens vazias não podem ser transporta­das junto com alimentos, bebidas, medicamentos, rações, animais e pessoas. Devem ser transporta­das em saco plástico transparente (Embalagens Padronizadas – modelo ABNT), devidamente identificado e com lacre, o qual deverá ser adqui­rido nos Canais de Distribuição.</div>','bioforca','adbio','adbio.png','2019-12-18 15:54:09','2019-12-18 15:54:09','Bioinseticida','Saco aluminizado sanfonado com capacidade de 1 Kg','Metarhizium','Controle da mosca branca','Saco aluminizado sanfonado com capacidade de 1 Kg');
/*!40000 ALTER TABLE `contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_has_contents`
--

DROP TABLE IF EXISTS `contents_has_contents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_has_contents` (
  `contents_id` int(11) NOT NULL,
  `contents_child_id` int(11) NOT NULL,
  PRIMARY KEY (`contents_id`,`contents_child_id`),
  KEY `fk_contents_has_contents_contents2_idx` (`contents_child_id`),
  KEY `fk_contents_has_contents_contents1_idx` (`contents_id`),
  CONSTRAINT `fk_contents_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contents_has_contents_contents2` FOREIGN KEY (`contents_child_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_has_contents`
--

LOCK TABLES `contents_has_contents` WRITE;
/*!40000 ALTER TABLE `contents_has_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_has_contents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contents_images`
--

DROP TABLE IF EXISTS `contents_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contents_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `type` varchar(45) DEFAULT NULL,
  `order` varchar(45) DEFAULT NULL,
  `image` text,
  `contents_id` int(11) NOT NULL,
  `path` text,
  PRIMARY KEY (`id`,`contents_id`),
  KEY `fk_contents_images_contents_idx` (`contents_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contents_images`
--

LOCK TABLES `contents_images` WRITE;
/*!40000 ALTER TABLE `contents_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `contents_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `informations`
--

DROP TABLE IF EXISTS `informations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `informations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `address` text,
  `number` varchar(45) DEFAULT NULL,
  `complement` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `phone1` varchar(45) DEFAULT NULL,
  `email` text,
  `about_us` text,
  `video` varchar(255) DEFAULT NULL,
  `opening_hours` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `informations`
--

LOCK TABLES `informations` WRITE;
/*!40000 ALTER TABLE `informations` DISABLE KEYS */;
INSERT INTO `informations` VALUES (1,'Rod. Assis Chateaubriand','s/n','Km 144 + 500m Conj. 02','Zona Rural','15400 000','Olímpia','SP','17 3279-4950','contato@bionatagro.com','A Bionat é uma fábrica de defensivos biológicos com parceiras com o Instituto Biológico de Campinas, Embrapa Recursos Genéticos e Biotecnologia (Cenargem) e a Empresa Brasileira de Pesquisa e Inovação Industrial (Embrapa).','https://www.youtube.com/watch?v=X6Xbpwpfn9w','Segunda a sexta das 8:00 às 18:00');
/*!40000 ALTER TABLE `informations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletters`
--

LOCK TABLES `newsletters` WRITE;
/*!40000 ALTER TABLE `newsletters` DISABLE KEYS */;
INSERT INTO `newsletters` VALUES (1,'sdsdsd@teste.com.br','2019-12-19 20:06:38','2019-12-19 20:06:38'),(2,'lemajstor@gmail.com','2019-12-19 20:07:18','2019-12-19 20:07:18'),(3,'edinaldo@agencialed.com.br','2019-12-19 20:15:56','2019-12-19 20:15:56'),(4,'atendimento@geo18k.com.br','2019-12-19 20:19:59','2019-12-19 20:19:59'),(5,'maniusa17@hotmail.com','2019-12-19 20:20:35','2019-12-19 20:20:35');
/*!40000 ALTER TABLE `newsletters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`(191))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('enzo.nagata@gmail.com','$2y$10$F87xZsXNmV3On.AEylyz3esP.VH3c4P7mZUpv0atsKI7D9KyLMvXS','2019-09-13 21:50:52');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `short_description` text NOT NULL,
  `content` text NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `image` varchar(45) NOT NULL DEFAULT 'null',
  `feat_image` varchar(45) NOT NULL DEFAULT 'null',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (20,'Kimberlit Agrociências inaugura fábrica no interior de São Paulo','A Kimberlit Agrociências ganha nova fábrica de defensivos biológicos Bionat. A inauguração ocorreu nesta terça-feira, no interior de São Paulo, na cidade de Olímpia.','<p>A Kimberlit Agrociências ganha nova fábrica de defensivos biológicos Bionat. A inauguração ocorreu nesta terça-feira, no interior de São Paulo, na cidade de Olímpia.</p><p>O mercado de fertilizantes deu um grande salto no ano de 2018, com faturamento de R$ 464,5 milhões e perspectiva de avanço de 20% no ano de 2019. Esse cenário influenciou na ampliação do parque fabril da Kimberlit.</p><p>A primeira fase de aportes será destinada ao desenvolvimento e a montagem da fábrica, serão gastos em torno de R$ 25 milhões. Para sua expansão R$ 20 milhões serão investidos e, por último, R$ 2,3 milhões virão da Financiadora de Estudos e Projetos (Finep).</p><p>Foi calculado pela empresa um aumento de 25% no faturamento para o ano de 2019, além da chegada de 32 novos produtos para o segmento. Com o mercado cada vez mais promissor, o número de produtos biológicos no Brasil disparou. Em 2010 haviam apenas 19 produtos biológicos no país e no início desse ano 200 produtos já estão disponíveis.&nbsp;</p>','kimberlit-agrociencias-inaugura-fabrica-no-interior-de-sao-paulo','post_5dfb8affb9002.jpeg','post_min_5dfb841cc851d.jpeg','2019-12-19 14:07:24','2019-12-19 14:36:47'),(21,'Entrevista no programa dia dia Rural, 19.07, canal terra viva','Luciano de Gissi, diretor da Bionat','<p>Entrevista no programa dia dia Rural, 19.07, canal terra viva</p><p>Luciano de Gissi, diretor da Bionat</p><p><iframe frameborder=\"0\" src=\"//www.youtube.com/embed/nVIC_wKcSCo\" width=\"640\" height=\"360\" class=\"note-video-clip\"></iframe><br></p><p><br></p>','entrevista-no-programa-dia-dia-rural,-19-07,-canal-terra-viva','post_5dfb87a038914.jpeg','post_min_5dfb87a037f25.jpeg','2019-12-19 14:22:24','2019-12-19 14:38:58'),(22,'Bionat é a Nova opção em Soluções Biológicas','A Bionat saiu em um artigo na revista Campos e Negócios  - Edição 201 / Dezembro 2019. Confira o artigo na íntegra.','<p>Kimberlit, empresa líder no segmento de fertilizantes especiais, especializada em nutrição e fisiologia de plantas, de olho em um mercado crescente, investe na produção de defensivos biológicos e inaugura a Bionat, fábrica que desenvolverá, produzirá e comercializará soluções biológicas inteligentes, sustentáveis e eficientes. Segundo dados da Associação Brasileira de Controle Biológico (ABC-Bio), o segmento faturou R$ 464,5 milhões em 2018 e a perspectiva é de crescer 20% em 2019.</p><p>“A Bionat surge a partir da visão de futuro do agronegócio mundial e das necessidades manifestadas pelos agropecuaristas de todas as regiões por produtos que potencializem os resultados no campo, e tenham um caráter sustentável.</p><p>Os números mostram que esse é um mercado de grande potencial; 57% dos produtores brasileiros dizem desconhecer os biodefensivos, de acordo com levantamento realizado pela ABC-Bio; temos um mercado extenso para desbravar”, afirma Luciano de Gissi, diretor na Bionat.</p><p>Foram cinco anos até que a nova empresa fosse concluída. Em 2016 o projeto foi minuciosamente estudado, pesquisado, trabalhado e detalhado com o auxílio de consultoria especializada e de pesquisadores do Instituto Biológico, da UFSCAR (Universidade Federal de São Carlos) e da ESALQ (Escola Superior de Agricultura da Universidade de São Paulo).</p><p>Com projeto arquitetônico e industrial aprovado em 2017, a primeira fase a unidade fabril da Bionat - localizada em Olímpia (SP) - está distribuída em 400 m2 de edificações com capacidade de produção de bioprodutos para tratamento de até 350 mil hectares por ano e contará com 8 funcionários.</p><p>A fábrica está aparelhada com equipamentos de tecnologia de ponta para a produção de fungos e bactérias, como um biorreator de última geração que pode ser controlado pelo celular, além de possuir processo de incubação e extração com alto grau de automação e oferecer um ambiente totalmente asséptico para reduzir a possibilidade de<br></p><p>contaminação – como parede revestida com tinta hospitalar antibacteriana, piso emborrachado e lavável, mobiliário produção em inox 316, com o objetivo promover um espaço saudável para a produção.</p><p>A Bionat está funcionando em fase de testes e tem previsão de início de produção em escala a partir agosto, com a liberação de registro feita pelo Mapa(- Ministério da Agricultura, Pecuária e Abastecimento), oferecendo ao mercado dois defensivos que têm como foco o combate às pragas cigarrinha e mosca-branca: o Metarhizium anisopliae,</p><p>fungo que é comprovadamente eficaz controlador biológico de várias espécies e cigarrinhas que ocorrem na agricultura, produzido em arroz, sob condição</p><p>de excelente de assepsia, rigoroso controle de qualidade a fim de garantir a ausência de contaminantes, eficiência na germinação e virulência dos conídios; e Beauveria bassiana, inseticida microbiológico indicado para o controle de insetos e ácaros pragas nas mais diversas culturas, tais como a mosca-branca, ácaro rajado, etc.</p><p><a href=\"http://local.bionat.com.br/bionat_artigo.pdf\" target=\"_blank\">Clique aqui para baxar o PDF do artigo</a><br></p>','bionat-e-a-nova-opcao-em-solucoes-biologicas','post_5dfbb8c68cfee.jpeg','post_min_5dfbb8c68c31d.jpeg','2019-12-19 17:52:06','2019-12-19 17:52:06');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Enzo Nagata','enzo.nagata@gmail.com',NULL,'$2y$10$HUuXgtcUoSVCKRnGZ7g4WOIogDOepbY4ZRb5M2ZMyJHWSvK9fMD0W','t95qJAaDIFb8MAzlxNaRAyeJqAZcJB43SsC5Bz7c3woDaNyvLsVg6v0rOqfM','2019-08-23 14:40:17','2019-08-23 14:40:17'),(2,'Edinaldo','edinaldo@agencialed.com.br',NULL,'$2y$10$HYROoUsNkqgAz65RW0Ct4uEX9vDIMH8KlQlmrKHigyRNfYUwzOUvG',NULL,'2019-09-13 22:19:59','2019-09-13 22:19:59'),(3,'admin','digital@agencialed.com.br',NULL,'$2y$10$qKyewisFIEyEgJSF5bTCFOtsKOyUFghosvMP1AkJBB4v6lPUchoa6',NULL,'2019-12-12 21:57:15','2019-12-12 21:57:15');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bionat'
--

--
-- Dumping routines for database 'bionat'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-19 17:24:36
