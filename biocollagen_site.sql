-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 27/11/2019 às 10:09
-- Versão do servidor: 10.2.27-MariaDB-log
-- Versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `biocollagen_site`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `info1` text DEFAULT NULL,
  `info2` text DEFAULT NULL,
  `info3` text DEFAULT NULL,
  `link1` text DEFAULT NULL,
  `link2` text DEFAULT NULL,
  `link3` text DEFAULT NULL,
  `path` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `banners`
--

INSERT INTO `banners` (`id`, `image`, `title`, `info1`, `info2`, `info3`, `link1`, `link2`, `link3`, `path`, `created_at`, `updated_at`, `status`, `order`) VALUES
(1, 'banner_5d77a509db7ee.jpeg', 'Banner 1', 'biomateriais <br> de colágeno', NULL, 'Conheça Nossos Produtos', NULL, NULL, '/produtos', NULL, '2019-09-10 13:22:38', '2019-09-27 20:40:34', NULL, NULL),
(4, 'banner_5d8e7432391b1.jpeg', 'Banner 2', 'biomateriais <br> de colágeno', NULL, 'Conheça Nossos Produtos', NULL, NULL, '/produtos', NULL, '2019-09-27 16:57:33', '2019-09-27 20:42:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `categories`
--

INSERT INTO `categories` (`id`, `title`, `url`, `type`) VALUES
(1, 'Qualquer coisa', 'qualquer-coisa', 'news'),
(2, 'Bio Ciências', 'bio-ciencias', 'news');

-- --------------------------------------------------------

--
-- Estrutura para tabela `categories_has_contents`
--

CREATE TABLE `categories_has_contents` (
  `categories_id` int(11) NOT NULL,
  `contents_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `categories_has_contents`
--

INSERT INTO `categories_has_contents` (`categories_id`, `contents_id`) VALUES
(1, 1),
(1, 2),
(2, 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `short_description` varchar(255) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `type` varchar(45) NOT NULL COMMENT 'post,news,page,page-name',
  `url` varchar(45) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `contents`
--

INSERT INTO `contents` (`id`, `title`, `description`, `short_description`, `content`, `type`, `url`, `image`, `created_at`, `updated_at`) VALUES
(6, 'Pericárdio Bovino', NULL, NULL, 'O pericárdio bovino é uma bolsa fibroserosa que reveste o coração. É revestida por uma camada túnica serosa e uma cama externa fibrosa. Ainda é formada por fibras elásticas entrelaçadas com uma camada densa de fibras colágenas.&nbsp;<div><br></div><div>O pericárdio bovino é utilizado há mais de 30 anos como matéria-prima na aplicação em diversas áreas medicinais, entre elas:</div><div>- Neurologia</div><div>- Urologia</div><div>- Cardiovascular</div><div>- Gastrenterologia</div><div>- Oftalmológica</div><div><br></div><div>O pericárdio bovino passa por tratamentos químicos e/ou enzimáticos antes da aplicação, isso faz com que ele:</div><div><br></div><div>- Aumente a força e flexibilidade</div><div>- Reduza a biodegradabilidade</div><div>- Torne-o biocompatível</div><div>- Seja não trombogênico</div><div><br></div>', 'produto', 'pericardio-bovino', '', '2019-09-16 18:50:08', '2019-09-25 20:21:13');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents_has_contents`
--

CREATE TABLE `contents_has_contents` (
  `contents_id` int(11) NOT NULL,
  `contents_child_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents_images`
--

CREATE TABLE `contents_images` (
  `id` int(11) NOT NULL,
  `description` text DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `order` varchar(45) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `contents_id` int(11) NOT NULL,
  `path` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `informations`
--

CREATE TABLE `informations` (
  `id` int(11) NOT NULL,
  `address` text DEFAULT NULL,
  `number` varchar(45) DEFAULT NULL,
  `complement` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(45) DEFAULT NULL,
  `instagram` text DEFAULT NULL,
  `facebook` text DEFAULT NULL,
  `linkedin` text DEFAULT NULL,
  `twitter` text DEFAULT NULL,
  `pinterest` text DEFAULT NULL,
  `phone1` varchar(45) DEFAULT NULL,
  `phone2` varchar(45) DEFAULT NULL,
  `email` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Despejando dados para a tabela `informations`
--

INSERT INTO `informations` (`id`, `address`, `number`, `complement`, `district`, `zipcode`, `city`, `state`, `whatsapp`, `instagram`, `facebook`, `linkedin`, `twitter`, `pinterest`, `phone1`, `phone2`, `email`) VALUES
(1, 'Av. João Batista Vetorasso', '805', 'Box 15', 'Distrito Industrial', NULL, 'São José do Rio Preto', 'SP', NULL, NULL, NULL, NULL, NULL, NULL, '+55 17 3308-9896', NULL, 'contato@biocollagen.technology');

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Despejando dados para a tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Estrutura para tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Despejando dados para a tabela `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('enzo.nagata@gmail.com', '$2y$10$F87xZsXNmV3On.AEylyz3esP.VH3c4P7mZUpv0atsKI7D9KyLMvXS', '2019-09-13 21:50:52');

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Despejando dados para a tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Enzo Nagata', 'enzo.nagata@gmail.com', NULL, '$2y$10$HUuXgtcUoSVCKRnGZ7g4WOIogDOepbY4ZRb5M2ZMyJHWSvK9fMD0W', 't95qJAaDIFb8MAzlxNaRAyeJqAZcJB43SsC5Bz7c3woDaNyvLsVg6v0rOqfM', '2019-08-23 14:40:17', '2019-08-23 14:40:17'),
(2, 'Edinaldo', 'edinaldo@agencialed.com.br', NULL, '$2y$10$HYROoUsNkqgAz65RW0Ct4uEX9vDIMH8KlQlmrKHigyRNfYUwzOUvG', NULL, '2019-09-13 22:19:59', '2019-09-13 22:19:59');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `categories_has_contents`
--
ALTER TABLE `categories_has_contents`
  ADD PRIMARY KEY (`categories_id`,`contents_id`),
  ADD KEY `fk_categories_has_contents_contents1_idx` (`contents_id`),
  ADD KEY `fk_categories_has_contents_categories1_idx` (`categories_id`);

--
-- Índices de tabela `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_UNIQUE` (`url`);

--
-- Índices de tabela `contents_has_contents`
--
ALTER TABLE `contents_has_contents`
  ADD PRIMARY KEY (`contents_id`,`contents_child_id`),
  ADD KEY `fk_contents_has_contents_contents2_idx` (`contents_child_id`),
  ADD KEY `fk_contents_has_contents_contents1_idx` (`contents_id`);

--
-- Índices de tabela `contents_images`
--
ALTER TABLE `contents_images`
  ADD PRIMARY KEY (`id`,`contents_id`),
  ADD KEY `fk_contents_images_contents_idx` (`contents_id`);

--
-- Índices de tabela `informations`
--
ALTER TABLE `informations`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `contents_images`
--
ALTER TABLE `contents_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `informations`
--
ALTER TABLE `informations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `contents_has_contents`
--
ALTER TABLE `contents_has_contents`
  ADD CONSTRAINT `fk_contents_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contents_has_contents_contents2` FOREIGN KEY (`contents_child_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
